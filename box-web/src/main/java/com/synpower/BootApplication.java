package com.synpower;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class BootApplication {
    public static void main(String[] args) {
        org.springframework.boot.SpringApplication.run(BootApplication.class, args);
    }
}
