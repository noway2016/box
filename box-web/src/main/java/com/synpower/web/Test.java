package com.synpower.web;

import com.synpower.service.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Test {
    @Autowired
    private DemoService demoService;
    @RequestMapping(value = "/hello",method = RequestMethod.GET)
    public String hello() {
        demoService.demo();
        return "222";
    }
}
